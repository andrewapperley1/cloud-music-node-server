
module.exports = {
    UPLOAD_MUSIC : "UPLOAD_MUSIC",
    UPLOAD_MUSIC_PROGRESS: "UPLOAD_MUSIC_PROGRESS",
    UPLOAD_MUSIC_FAILURE : "UPLOAD_MUSIC_FAILURE",
    UPLOAD_MUSIC_SUCCESS : "UPLOAD_MUSIC_SUCCESS",

    LIST_MUSIC : "LIST_MUSIC",
    LIST_MUSIC_FAILURE : "LIST_MUSIC_FAILURE",
    LIST_MUSIC_SUCCESS : "LIST_MUSIC_SUCCESS",

    LOAD_USER: "LOAD_USER",
    LOAD_USER_FAILURE : "LOAD_USER_FAILURE",
    LOAD_USER_SUCCESS : "LOAD_USER_SUCCESS",

    LOAD_USERS: "LOAD_USERS",
    LOAD_USERS_FAILURE : "LOAD_USERS_FAILURE",
    LOAD_USERS_SUCCESS : "LOAD_USERS_SUCCESS",

    UPDATE_USER: "UPDATE_USER",
    UPDATE_USER_FAILURE : "UPDATE_USER_FAILURE",
    UPDATE_USER_SUCCESS : "UPDATE_USER_SUCCESS",

    LOGOUT : "LOGOUT",
    LOGOUT_FAILURE : "LOGOUT_FAILURE",
    LOGOUT_SUCCESS : "LOGOUT_SUCCESS"

};